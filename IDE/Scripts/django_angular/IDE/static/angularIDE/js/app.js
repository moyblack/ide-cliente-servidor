var module = angular.module("IDE", ['ngRoute','ui.ace']);
module.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
})
.config(function($locationProvider)
{
    $locationProvider.hashPrefix('');
});
module.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: static_url + 'angularIDE/views/ide.html',
                controller: 'ctrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);

module.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http,  $interval) {
  
   $scope.Acethemes = [
    "chrome",
    "clouds",
    "crimson_editor",
    "dawn",
    "dreamweaver",
    "eclipse",
    "github",
    "solarized_light",
    "textmate",
    "tomorrow",
    "xcode",
    "kuroir",
    "katzenmilch",
    "ambiance",
    "chaos",
    "clouds_midnight",
    "cobalt",
    "idle_fingers",
    "kr_theme",
    "merbivore",
    "merbivore_soft",
    "mono_industrial",
    "monokai",
    "pastel_on_dark",
    "solarized_dark",
    "terminal",
    "tomorrow_night",
    "tomorrow_night_blue",
    "tomorrow_night_bright",
    "tomorrow_night_eighties",
    "twilight",
    "vibrant_ink",
  ];
  $scope.themes = [
    {id:0, theme:"chrome"},
    {id:1, theme:"clouds"},
    {id:2, theme:"crimson_editor"},
    {id:3, theme:"dawn"},
    {id:4, theme:"dreamweaver"},
    {id:5, theme:"eclipse"},
    {id:6, theme:"github"},
    {id:7, theme:"solarized_light"},
    {id:8, theme:"textmate"},
    {id:9, theme:"tomorrow"},
    {id:10, theme:"xcode"},
    {id:11, theme:"kuroir"},
    {id:12, theme:"katzenmilch"},
    {id:13, theme:"ambiance"},
    {id:14, theme:"chaos"},
    {id:15, theme:"cobalt"},
    {id:16, theme:"idle_fingers"},
    {id:17, theme:"kr_theme"},
    {id:18, theme:"merbivore"},
    {id:19, theme:"merbivore_soft"},
    {id:20, theme:"mono_industrial"},
    {id:21, theme:"clouds_midnight"},
    {id:22, theme:"monokai"},
    {id:23, theme:"pastel_on_dark"},
    {id:24, theme:"solarized_dark"},
    {id:25, theme:"terminal"},
    {id:26, theme:"tomorrow_night"},
    {id:27, theme:"tomorrow_night_blue"},
    {id:28, theme:"tomorrow_night_bright"},
    {id:29, theme:"tomorrow_night_eighties"},
    {id:30, theme:"twilight"},
    {id:31, theme:"vibrant_ink"},
  ];
  
  var modes = [
    'abap',
    'actionscript',
    'ada',
    'apache_conf',
    'asciidoc',
    'assembly_x86',
    'autohotkey',
    'batchfile',
    'c9search',
    'c_cpp',
    'cirru',
    'clojure',
    'cobol',
    'coffee',
    'coldfusion',
    'csharp',
    'css',
    'curly',
    'd',
    'dart',
    'diff',
    'dockerfile',
    'dot',
    'dummy',
    'dummysyntax',
    'eiffel',
    'ejs',
    'elixir',
    'elm',
    'erlang',
    'forth',
    'ftl',
    'gcode',
    'gherkin',
    'gitignore',
    'glsl',
    'golang',
    'groovy',
    'haml',
    'handlebars',
    'haskell',
    'haxe',
    'html',
    'html_ruby',
    'ini',
    'io',
    'jack',
    'jade',
    'java',
    'javascript',
    'json',
    'jsoniq',
    'jsp',
    'jsx',
    'julia',
    'latex',
    'less',
    'liquid',
    'lisp',
    'livescript',
    'logiql',
    'lsl',
    'lua',
    'luapage',
    'lucene',
    'makefile',
    'markdown',
    'matlab',
    'mel',
    'mushcode',
    'mysql',
    'nix',
    'objectivec',
    'ocaml',
    'pascal',
    'perl',
    'pgsql',
    'php',
    'powershell',
    'praat',
    'prolog',
    'properties',
    'protobuf',
    'python',
    'r',
    'rdoc',
    'rhtml',
    'ruby',
    'rust',
    'sass',
    'scad',
    'scala',
    'scheme',
    'scss',
    'sh',
    'sjs',
    'smarty',
    'snippets',
    'soy_template',
    'space',
    'sql',
    'stylus',
    'svg',
    'tcl',
    'tex',
    'text',
    'textile',
    'toml',
    'twig',
    'typescript',
    'vala',
    'vbscript',
    'velocity',
    'verilog',
    'vhdl',
    'xml',
    'xquery',
    'yaml',
];
  
    $scope.value = 'print "Hola Mundo"';
    $scope.currentTheme = 14;

  
  $scope.aceLoaded = function (_editor) {
    
    _editor.setFontSize(14);
    _editor.$blockScrolling = Infinity;
    _editor.setShowPrintMargin(false);
    
  }
  
  $scope.aceChanged = function (_editor) {
    
  }
  
  var editor = function() {
    this.theme = $scope.Acethemes[$scope.currentTheme];
    this.mode = 'python';
    this.opacity = 100;
    this.useWrapMode = true;
    this.gutter = true;
    this.splitMode = false;
    this.themeCycle = true;
  };


  $scope.editor = new editor();

  $scope.actTheme = function()
  {
    $scope.editor.theme = $scope.Acethemes[$scope.selectedTheme.id];
  }


  /**
  var gui = new dat.GUI();
  
  var opacityCtrl = gui.add($scope.editor, 'opacity', 0, 100);
  opacityCtrl.onChange(function(value) {
    $scope.$apply();
  });
  
  var themeController = gui.add($scope.editor, 'theme', $scope.themes).listen();
  themeController.onChange(function(value) {
    $scope.$apply();
  });
  
  var modeController = gui.add($scope.editor, 'mode', modes);
  modeController.onChange(function(value) {
    $scope.$apply();
  });
  
  var useWrapModeCtrl = gui.add($scope.editor, 'useWrapMode', true);
  useWrapModeCtrl.onChange(function(value) {
    $scope.$apply();
  });
  
  var gutterCtrl = gui.add($scope.editor, 'gutter', false);
  gutterCtrl.onChange(function(value) {
    $scope.$apply();
  });
  
  var splitMode = gui.add($scope.editor, 'splitMode', false);
  splitMode.onChange(function(value) {
    $scope.$apply();
  });
  
  $scope.themeCycle = function() {
        if($scope.editor.themeCycle) {
          $scope.currentTheme = $scope.currentTheme + 1;
          if($scope.currentTheme < themes.length) {
            $scope.editor.theme = themes[$scope.currentTheme];
          } else {
            $scope.currentTheme = 0;
            $scope.editor.theme = themes[$scope.currentTheme];
          }
        }
  }
  */
  
}]);