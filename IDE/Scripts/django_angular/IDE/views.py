# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.conf import settings

# Create your views here.
def home(req):
    return render(req, 'main.html', {'STATIC_URL': settings.STATIC_URL})